import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { IMovie } from '../../models/IMovie';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  public activeMovie: any;
  public genreList: any;
  public relMovies: IMovie[];


  constructor(private actvRoute: ActivatedRoute,
              public shService: SharedService,
              public router: Router) {

  }

  setGenres(): void {
    this.genreList = this.shService.getGenresList();
  }

  setMovieFromParam(parmMovie: {[key: string]: any}): void {
    if (parmMovie.title) {
      this.shService.getMovie(parmMovie.title)
          .subscribe( (movie: IMovie) => {
              if (!movie) {
                this.router.navigateByUrl('/not-found');
              } else {
                this.activeMovie = movie;
                this.setRelMovies(movie);
                window.scroll({top: 0, behavior: 'smooth'});
              }
          });
    }
  }

  setRelMovies(movie: IMovie) {
    if (! movie) {  return false; }
    this.shService.getAllMovies()
                  .pipe(map((dataMovies: IMovie[]) => {
                    return dataMovies.reduce( (acu, cur: IMovie) => {
                      const isEqual = cur.genres.sort()
                                                .every( (item, ind) => item === movie.genres.sort()[ind] );
                      if (isEqual) { acu.push(cur); }
                      return acu;
                    }, [] as IMovie[]);
                  }))
                  .subscribe( (item: IMovie[]) =>  this.relMovies = item);
  }

  ngOnInit() {
    this.setGenres();
    this.actvRoute.params.subscribe( par => {
      this.setMovieFromParam(par);
    });

  }



}

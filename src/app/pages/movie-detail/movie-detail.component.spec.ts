import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { movies } from './../../off-line-data/data/movie-database';
import { MovieDetailComponent } from './movie-detail.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of, Observable } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { FilterGenresPipe } from '../../pipes/filter-genres.pipe';
import { SharedService } from '../../services/shared.service';

describe('MovieDetailComponent', () => {
  let component: MovieDetailComponent;
  let fixture: ComponentFixture<MovieDetailComponent>;
  let service: SharedService;
  const mockGenres = ['action', 'adventure', 'biography', 'comedy', 'crime', 'drama', 'history', 'mystery', 'scifi', 'sport', 'thriller'];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ MovieDetailComponent, FilterGenresPipe ],
      providers: [ {
        provide: ActivatedRoute,
        useValue:  { params: of({title: 'deadpool'}) }
      } ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
    service = new SharedService();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create detail', () => {
    expect(component).toBeTruthy();
  });

  it('should deadpool param load the movie', async(() => {
    const findDeadpool = movies.find(movie => movie.key === 'deadpool');
    spyOn(service, 'getMovie').and.returnValue( of([movies[0]]));
    const result = component.setMovieFromParam({title: 'deadpool'});
    fixture.detectChanges();
    expect(component.activeMovie).toEqual(findDeadpool);
  }));

  it('should set the GenresList', async(() => {
    spyOn(service, 'getGenresList').and.returnValue(mockGenres);
    component.setGenres();
    expect(component.genreList).toEqual(mockGenres);
  }));



});

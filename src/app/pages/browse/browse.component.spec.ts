import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowseComponent } from './browse.component';
import { IMovie } from '../../models/IMovie';
import { movies } from './../../off-line-data/data/movie-database';
import { ListItemsComponent } from '../../generic-components/list-items/list-items.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { FilterPipe } from '../../pipes/filter.pipe';
import { FilterGenresPipe } from '../../pipes/filter-genres.pipe';
import { FilterRatingPipe } from '../../pipes/filter-rating.pipe';

describe('BrowseComponent', () => {
  let component: BrowseComponent;
  let fixture: ComponentFixture<BrowseComponent>;
  let service: SharedService;
  const mockGenres = ['action', 'adventure', 'biography', 'comedy', 'crime', 'drama', 'history', 'mystery', 'scifi', 'sport', 'thriller'];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseComponent, ListItemsComponent, FilterPipe, FilterGenresPipe, FilterRatingPipe ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
    service = new SharedService();
  }));

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [ BrowseComponent, ListItemsComponent]
    }).createComponent(BrowseComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
    service = new SharedService();
  });

  it('should component exist', () => {
    expect(component).toBeTruthy();
  });

  it('should load a local moviesList', async(() => {
    spyOn(service, 'isOnline').and.returnValue(false);
    const mockMovies: IMovie[] =  movies;
    component.getMovies();
    expect(component.movieData).toEqual(mockMovies);
  }));

  it('should load a GenresList', async(() => {
    spyOn(service, 'getGenresList').and.returnValue(mockGenres);
    component.getGenres();
    expect(component.genreList).toEqual(mockGenres);
  }));


});

import { Component, OnInit } from '@angular/core';
import { movies } from './../../off-line-data/data/movie-database';
import { IMovie } from '../../models/IMovie';
import { SharedService } from '../../services/shared.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})
export class BrowseComponent implements OnInit {

  private offlineData: IMovie[] = movies;
  public movieData: IMovie[];
  public genreList: string[];

  constructor(public shService: SharedService) {
  }

  getMovies() {
    if ( this.shService.isOnline() ) {
      // Look on some API;
      return null;
    } else {
        this.shService.getAllMovies().subscribe( item => this.movieData = item);
    }
  }

  getGenres() {
    this.genreList = this.shService.getGenresList();
  }


  ngOnInit() {
    this.getMovies();
    this.getGenres();
  }

}

import { FilterRatingPipe } from './filter-rating.pipe';
import { movies } from '../off-line-data/data/movie-database';
import { GenreType } from '../models/movie.model';

describe('FilterRatingPipe', () => {
  let pipe: FilterRatingPipe;
  let result,
      transformed;


  beforeEach(() => {
    pipe = new FilterRatingPipe();
  });

  it('create an instance', () => {
    result = null;
    transformed = null;
    expect(pipe).toBeTruthy();
  });

  it('Should return Batman from rate >= 9', () => {
    result = {
      id: 20,
      key: 'the-dark-knight',
      name: 'The Dark Knight',
      description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.',
      genres: [GenreType.Action, GenreType.Crime, GenreType.Drama],
      rate: '9.0',
      length: '2hr 32mins',
      img: 'the-dark-knight.jpg'
    };

    transformed = pipe.transform(movies, 9);

    expect(pipe).toBeTruthy([result]);
  });
});

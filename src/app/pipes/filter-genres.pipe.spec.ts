import { FilterGenresPipe } from './filter-genres.pipe';
import { movies } from '../off-line-data/data/movie-database';
import { GenreType } from '../models/movie.model';

describe('FilterGenresPipe', () => {
  const localMovies = movies;
  let pipe: FilterGenresPipe;
  let transformed: any ;
  let result: any ;
  let genres: any[];

  beforeEach(() => {
    pipe = new FilterGenresPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('create an return actions movies', () => {
    genres = [{'title': 'scifi', 'active': true}];
    transformed = pipe.transform(localMovies, genres);
    result = [
      {
        id: 8,
        key: "jurassic-world",
        name: "Jurassic World",
        description: "A new theme park is built on the original site of Jurassic Park. Everything is going well until the park's newest attraction--a genetically modified giant stealth killing machine--escapes containment and goes on a killing spree.",
        genres: [GenreType.Action, GenreType.Adventure, GenreType.Scifi],
        rate: "7.1",
        length: "2hr 4mins",
        img: "jurassic-world.jpg"
      },

      {
        id: 14,
        key: "ant-man",
        name: "Ant-Man",
        description: "Armed with a super-suit with the astonishing ability to shrink in scale but increase in strength, cat burglar Scott Lang must embrace his inner hero and help his mentor, Dr. Hank Pym, plan and pull off a heist that will save the world.",
        genres: [GenreType.Action, GenreType.Adventure, GenreType.Scifi],
        rate: "7.4",
        length: "1hr 57mins",
        img: "ant-man.jpg"
      },
      {
        id: 22,
        key: "the-matrix",
        name: "The Matrix",
        description: "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",
        genres: [GenreType.Action, GenreType.Scifi],
        rate: "8.7",
        length: "2hr 16mins",
        img: "the-matrix.jpg"
      }
    ];
    expect(transformed).toEqual(result);
  });


});


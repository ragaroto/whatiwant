import { Pipe, PipeTransform } from '@angular/core';
import { IMovie } from '../models/IMovie';

@Pipe({
  name: 'filterGenres',
  pure: false
})

export class FilterGenresPipe implements PipeTransform {
  lista: Array<any>;
  retorno: any ;

  transform(movies: any[], genres: any[]): any[] {
    let it;
    if (movies ) {
      it = movies.filter( (movie: IMovie) => {
          if (!movie.genres) {
            return false;
          } else {
            // Filter to be Exatly Equal
            // const movieA = movie.genres.sort();
            // const movieB = genres.filter(selectedGenres =>  selectedGenres.active === true ).map( genre => genre.title).sort();
            // const isEqual = movieA.every( (movieAitem, ind) => movieAitem === movieB[ind]);
            // return isEqual ? true : false ;

            // Filter to have at leat one match
            return movie.genres.some( movGenres => {
              const filteredGenres = genres.filter(selectedGenres =>  selectedGenres.active );
              return filteredGenres ? filteredGenres.some( item => item.title === movGenres) : false ;
            });
          }
      });
    } else {
      it = [];
    }
    return it;
  }
}

import { FilterPipe } from './filter.pipe';
import { movies } from './../off-line-data/data/movie-database';
import { GenreType } from '../models/movie.model';

describe('FilterPipe', () => {
  const localMovies = movies;
  let pipe: FilterPipe;
  let transformed: any ;
  let result: any ;

  beforeEach(() => {
    pipe = new FilterPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pipe return array deadpool', () => {
    transformed = pipe.transform(localMovies, 'deadpool');
    result = [{
      id: 1,
      key: 'deadpool',
      name: 'Deadpool',
      description: 'A former Special Forces operative turned mercenary is subjected to a rogue experiment that leaves him with accelerated healing powers, adopting the alter ego Deadpool.',
      genres: [GenreType.Action, GenreType.Adventure, GenreType.Comedy],
      rate: '8.6',
      length: '1hr 48mins',
      img: 'deadpool.jpg'
    }];
    expect(transformed).toEqual(result);
  });

  it('should pipe return array of american-gangster and gangster-squad', () => {
    transformed = pipe.transform(localMovies, 'gangster');
    result = [{
      id: 5,
      key: "american-gangster",
      name: "American Gangster",
      description: "In 1970s America, a detective works to bring down the drug empire of Frank Lucas, a heroin kingpin from Manhattan, who is smuggling the drug into the country from the Far East.",
      genres: [GenreType.Biography, GenreType.Crime, GenreType.Drama],
      rate: "7.8",
      length: "2hr 37mins",
      img: "american-gangster.jpg"
    },
    {
      id: 6,
      key: "gangster-squad",
      name: "Gangster Squad",
      description: "It's 1949 Los Angeles, the city is run by gangsters and a malicious mobster, Mickey Cohen. Determined to end the corruption, John O'Mara assembles a team of cops, ready to take down the ruthless leader and restore peace to the city.",
      genres: [GenreType.Action, GenreType.Crime, GenreType.Drama],
      rate: "6.8",
      length: "1hr 53mins",
      img: "gangster-squad.jpg"
    }];
    expect(transformed).toEqual(result);
  });

});

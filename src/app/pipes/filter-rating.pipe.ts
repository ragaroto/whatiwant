import { Pipe, PipeTransform } from '@angular/core';
import { IMovie } from '../models/IMovie';

@Pipe({
  name: 'filterRating'
})
export class FilterRatingPipe implements PipeTransform {

  transform(movies: IMovie[], rate: number): any {
    return movies.filter( (movie: IMovie) => {
      return  movie.rate ? (rate < parseInt(movie.rate, 10)) : false;
    });
  }

}

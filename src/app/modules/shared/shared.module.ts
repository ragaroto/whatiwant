import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatToolbarModule,
  MatIconModule,
  MatSnackBarModule,
  MatTabsModule,
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule,
  MatSidenavModule,
  MatListModule,
  MatProgressBarModule,
  MatChipsModule,
  MatDividerModule,
  MatCardModule,
  MatExpansionModule,
  MatSliderModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatSnackBarModule,
    MatSelectModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatChipsModule,
    MatDividerModule,
    MatCardModule,
    MatExpansionModule,
    MatSliderModule
  ],
  declarations: [],
  exports: [
    CommonModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatSnackBarModule,
    MatSelectModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatChipsModule,
    MatDividerModule,
    MatCardModule,
    MatExpansionModule,
    MatSliderModule
  ]
})
export class SharedModule { }

import { Component, OnInit, Input } from '@angular/core';
import { IMovie } from '../../models/IMovie';
import { SharedService } from '../../services/shared.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-movie-full',
  templateUrl: './movie-full.component.html',
  styleUrls: ['./movie-full.component.scss']
})
export class MovieFullComponent implements OnInit {

  public movie: IMovie;
  public obsMovie = new BehaviorSubject<IMovie>(null);

  @Input('behaveMovie')
  set obsObservable(value) {
    this.obsMovie.next(value);
  }


  public staticURL: string;

  constructor( public shShared: SharedService) { }

  setBlurredBG() {
    const styleElem = document.head.appendChild(document.createElement('style'));
    styleElem.innerHTML = `.box-movie:before {background: url(${this.staticURL})}`;
  }

  ngOnInit() {
      this.obsMovie.subscribe( data => {
        this.movie = data;
        if (!this.movie) {
          return false;
        }
        this.staticURL = this.shShared.getCoversFolder(this.movie.img, null);
        this.setBlurredBG();
      });
  }

}

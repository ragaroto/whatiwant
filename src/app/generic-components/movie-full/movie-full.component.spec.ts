import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieFullComponent } from './movie-full.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedService } from '../../services/shared.service';

describe('MovieFullComponent', () => {
  let component: MovieFullComponent;
  let fixture: ComponentFixture<MovieFullComponent>;
  let service: SharedService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieFullComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
    service = new SharedService();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieFullComponent);
    component = fixture.componentInstance;
    component.movie = {
      id: 1,
      key: 'a',
      name: '',
      description: '',
      genres: null,
      rate: '',
      length: '',
      img: ''
    };
    fixture.detectChanges();
  });

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
});

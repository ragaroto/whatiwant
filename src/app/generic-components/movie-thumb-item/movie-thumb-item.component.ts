import { Component, OnInit, Input } from '@angular/core';
import { IMovie } from '../../models/IMovie';
import { SharedService } from '../../services/shared.service';


@Component({
  selector: 'app-movie-thumb-item',
  templateUrl: './movie-thumb-item.component.html',
  styleUrls: ['./movie-thumb-item.component.scss']
})
export class MovieThumbItemComponent implements OnInit {

  @Input() movie: IMovie;
  public staticURL: string;

  constructor( public shShared: SharedService) { }

  ngOnInit() {
      this.staticURL = this.shShared.getCoversFolder(this.movie.img, null);

  }

}

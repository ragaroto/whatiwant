import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieThumbItemComponent } from './movie-thumb-item.component';
import { movies } from './../../off-line-data/data/movie-database';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { IMovie } from '../../models/IMovie';

describe('MovieThumbItemComponent', () => {
  let component: MovieThumbItemComponent;
  let fixture: ComponentFixture<MovieThumbItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieThumbItemComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieThumbItemComponent);
    component = fixture.componentInstance;
    const mockMovies: IMovie[] =  movies;
    component.movie = mockMovies[1];
    fixture.detectChanges();
  });

  it('should existe teste', async(() => {
    expect(component).toBeTruthy();
  }));
});

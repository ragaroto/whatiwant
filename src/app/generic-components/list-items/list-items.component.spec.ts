import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemsComponent } from './list-items.component';
import { MovieThumbItemComponent } from '../movie-thumb-item/movie-thumb-item.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FilterPipe } from '../../pipes/filter.pipe';
import { FilterGenresPipe } from '../../pipes/filter-genres.pipe';
import { FilterRatingPipe } from '../../pipes/filter-rating.pipe';
import { movies } from '../../off-line-data/data/movie-database';
import { SharedService } from '../../services/shared.service';

describe('ListItemsComponent Tests', () => {
  let component: ListItemsComponent;
  let fixture: ComponentFixture<ListItemsComponent>;
  let service: SharedService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemsComponent, MovieThumbItemComponent, FilterPipe, FilterGenresPipe, FilterRatingPipe ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
    service = new SharedService();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(ListItemsComponent);
    component = fixture.componentInstance;
    component.filterGenres = ['action', 'adventure', 'biography', 'comedy', 'crime', 'drama', 'history', 'mystery', 'scifi', 'sport', 'thriller'];
    component.movieList = movies;
    service = new SharedService();
    fixture.detectChanges();
  });

  // it('should component exist', () => {
  //   expect(component).toBeTruthy();
  // });

  it('should render app-movie-thumb-item', async(() => {
    spyOn(service, 'isOnline').and.returnValue(true);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const hasItem = compiled.querySelector('app-movie-thumb-item');
    expect(hasItem).toBeTruthy();
  }));

  it('should have same length li === movies.length', async(() => {
    spyOn(service, 'isOnline').and.returnValue(true);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const lengthItens = compiled.querySelectorAll('.lista-items li').length;
    expect(lengthItens).toEqual(movies.length);
  }));
});

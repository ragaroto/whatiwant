import { Component, OnInit, Input } from '@angular/core';
import { IMovie } from '../../models/IMovie';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {

  @Input() movieList: IMovie[];
  @Input() filterGenres: string[];
  @Input() isFilterable: boolean;
  public searchText: string;
  public selectedGenres: any[];
  public selectedRate: number;

  constructor() { }

  toggleMenuMobile(): void {
    const elms = document.querySelectorAll('.menuMobile'),
          lth = elms.length;
    if (lth > 0) {
      for (let i = 0; i < lth; i++) {
        elms[i].classList.contains('isHidden-small-devices')
          ? elms[i].classList.remove('isHidden-small-devices')
          : elms[i].classList.add('isHidden-small-devices');
      }
    }
  }

  initFilters(): void {
    this.selectedGenres = this.filterGenres.map( genre => {
      return { title: genre, active: true};
    });
  }

  isAllChecked() {
    if (this.selectedGenres.every( item => item.active === true )) {
      return true;
    } else {
      return false;
    }
  }

  selectGenre(genre: any) {
    if (this.selectedGenres.every( item => item.active === true )) {
        this.deslectAllGenres();
        genre.active = true;
    } else {
      genre.active = genre.active  ? false : true;
    }
  }
  deslectAllGenres(): void {
    this.selectedGenres.map( item => {
      item.active = false;
      return item;
    });
  }
  selectAllGenres(): void {

    if (this.selectedGenres.every( item => item.active === true )) {
      this.deslectAllGenres();
    } else {
      this.selectedGenres.map( item => {
        item.active = true;
        return item;
      });
    }
  }



  ngOnInit() {
    this.selectedRate = 3;
    if (this.isFilterable) {
      this.initFilters();
    }
  }

}

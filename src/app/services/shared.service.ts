import { Injectable } from '@angular/core';
import { movies } from './../off-line-data/data/movie-database';
import { IMovie } from './../models/IMovie';
import { of, Observable } from 'rxjs';
import { GenreType } from '../models/movie.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  Online = false;
  localImagesPath = './../../app/off-line-data/images/covers/';
  serverImagesPath = 'http://via.placeholder.com/';
  localMovies = movies;
  genre = GenreType;

  constructor() { }

  getCoversFolder(imgName: string, size?: string): string {
    const tsize = size ? size : '';
    if (this.isOnline()) {
      return this.serverImagesPath + tsize + '?text=' +  imgName.replace(/ /g , '+');
    } else {
      return this.localImagesPath + tsize  + imgName.replace(/ /g, '');
    }
  }
  isOnline(): boolean {
    return this.Online;
  }
  beOnline(): void {
    this.Online = true;
  }
  beOffline(): void {
    this.Online = false;
  }

  getGenresList(): string[] {
    const cat: string[] = [];
    for (const n in this.genre) {
        if (typeof this.genre[n] === 'string') {
          cat.push(this.genre[n]);
        }
    }
    return cat;
  }

  getAllMovies(): Observable<IMovie[]> {
    return of(this.localMovies);
  }

  getMovie( name: string): Observable<IMovie> {
    return new Observable(observer => {
      this.getAllMovies().subscribe( (allMovies: IMovie[]) => {
        const result = allMovies.find( movie => name === movie.key);
        if (result) {
          observer.next(result);
        } else {
          observer.next(null);
        }
      });
    });
  }
}

import { TestBed, inject, async } from '@angular/core/testing';

import { SharedService } from './shared.service';
import { GenreType } from '../models/movie.model';
import { of, Observable } from 'rxjs';
import { IMovie } from '../models/IMovie';

describe('SharedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedService]
    });
  });

  it('should service exist', inject([SharedService], (service: SharedService) => {
    expect(service).toBeTruthy();
  }));

  it('should isOnline be different to undefined', inject([SharedService], (service: SharedService) => {
    expect(service.isOnline()).not.toBeUndefined();
  }));

  it('should beOffline change isOnline to false', inject([SharedService], (service: SharedService) => {
    service.beOffline();
    const result = service.isOnline();
    expect(result).toBeFalsy();
  }));

  it('should beOnline change isOnline to true', inject([SharedService], (service: SharedService) => {
    service.beOnline();
    const result = service.isOnline();
    expect(result).toBeTruthy();
  }));

  it('should getCoversFolder return offlinePath', inject([SharedService], (service: SharedService) => {
    service.beOffline();
    const result = service.getCoversFolder('nome', null);
    expect(result).toEqual('./../../app/off-line-data/images/covers/nome');
  }));

  it('should getCoversFolder return offlinePath', inject([SharedService], (service: SharedService) => {
    service.beOnline();
    const result = service.getCoversFolder('nome', null);
    expect(result).toEqual('http://via.placeholder.com/?text=nome');
  }));


  it('should return all Genres', inject([SharedService], (service: SharedService) => {
    const result = service.getGenresList();
    const expected = ['action', 'adventure', 'biography', 'comedy', 'crime', 'drama', 'history', 'mystery', 'scifi', 'sport', 'thriller'];
    expect(result).toEqual(expected);
  }));

  it('should return an observable of movie', async( inject([SharedService], (service: SharedService) => {
    let result: IMovie;
    let expected: IMovie;

        service.getMovie('deadpool').subscribe(item => result = item);
        expected = {
        id: 1,
        key: 'deadpool',
        name: 'Deadpool',
        description: 'A former Special Forces operative turned mercenary is subjected to a rogue experiment that leaves him with accelerated healing powers, adopting the alter ego Deadpool.',
        genres: [GenreType.Action, GenreType.Adventure, GenreType.Comedy],
        rate: '8.6',
        length: '1hr 48mins',
        img: 'deadpool.jpg'
      };
    expect(result).toEqual(expected);


  })));




});

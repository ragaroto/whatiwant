import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { BrowseComponent } from './pages/browse/browse.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { MovieDetailComponent } from './pages/movie-detail/movie-detail.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'browse',
        pathMatch: 'full'
    },
    {
        path: 'browse',
        component: BrowseComponent
    },
    {
        path: 'movie-detail/:title',
        component: MovieDetailComponent
    },

    { path: 'not-found',     component: NotfoundComponent},
    { path: '**',       component: NotfoundComponent }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes, {useHash: false});

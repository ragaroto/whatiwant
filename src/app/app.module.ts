import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';

import { RouterModule } from '@angular/router';

/* Importando Rotas */
import { Routing } from './routes';

import { NotfoundComponent } from './pages/notfound/notfound.component';
import { BrowseComponent } from './pages/browse/browse.component';
import { MovieDetailComponent } from './pages/movie-detail/movie-detail.component';
import { TopMenuComponent } from './generic-components/top-menu/top-menu.component';
import { ListItemsComponent } from './generic-components/list-items/list-items.component';
import { MovieThumbItemComponent } from './generic-components/movie-thumb-item/movie-thumb-item.component';
import { MovieFullComponent } from './generic-components/movie-full/movie-full.component';
import { FilterPipe } from './pipes/filter.pipe';
import { FilterGenresPipe } from './pipes/filter-genres.pipe';
import { FilterRatingPipe } from './pipes/filter-rating.pipe';


@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    BrowseComponent,
    MovieDetailComponent,
    TopMenuComponent,
    ListItemsComponent,
    MovieThumbItemComponent,
    MovieFullComponent,
    FilterPipe,
    FilterGenresPipe,
    FilterRatingPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    SharedModule,
    RouterModule,
    Routing
  ],
  providers: [FilterGenresPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
